# Tokebot V1 [NO LONGER IN USE]
Tokebot for fore.st, made with ChozoBot.

### This version of tokebot is no longer in use, as tokebot functionality was ported to the main [fore.st](https://gitlab.com/rainbownapkin/fore.st) codebase as part of fore.st V1.1 Pineapple Express. This version of Tokebot is based on a cytube compatible bot modded for fore.st v1.0 and is **NOT** compatible with modern versions of fore.st, though _should_ be compatible with modern cytube (untested). This repo will remain in place for archival purposes.

## Requirements

  - nodejs (12+, last tested on 12.18.2)

## Setup

I'll get around to writing this readme properly, but:

Copy the config.example.js file and name it config.js. If you're providing a room parameter to the bot (see below), name it config-roomname.js instead.

Read through the configuration file and carefully make sure everything is set just right.

Install the node modules: `npm install`

## Usage

Run the bot: `node .`

You can provide a room parameter: `node . -r room` if you'd like to have different configurations for different rooms.

You may also use the scripts provided to allow the bot to restart if it is killed. Replace `node` with `./start.sh`, for example. However, the batch (Windows) script was not tested since it was last edited.

It is recommended to have the bot at rank 3 (Admin) so it has full functionality.

When updating the bot, you must make sure to add any new configuration lines every time.

To create custom commands, see customcommands.example.js for writing them, and the bottom of the config.js file for more info on including them.
