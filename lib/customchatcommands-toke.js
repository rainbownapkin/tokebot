"use strict";

/*
  Use this file to define custom commands, especially room-centric ones.
  Try to avoid editing chatcommands.js so future updates won't erase your edits.

  Rename this file to customchatcommands.js to use it, OR if the advanced
  configuration setting "useChannelCustomCommands" is true, rename this to
  customchatcommands-roomname.js instead.

  You can also rename this to customchatcommands-(anything).js and edit your
  configuration file accordingly. Within your config, refer to:
    advanced.customCommandsToLoad
  There is more information in the configuration file on how to set this up.

  See chatcommands.js for more information on creating commands.
*/

const C = require("cli-color");

const utils = require("./utils.js");
const strings = require("./strings.js");
const api = require("./api.js");
const Command = require("./chatcommands.js").Command;
let toking = 0;//0 ready to start toke, 1 toking, 2 cooldown active
let tokers = [];
let cdown = 3;
let cdel = 120;
let ctime = cdel;
let solotokes = ["", "https://ourfore.st/img/femotes/onetoker.jpg","https://ourfore.st/img/femotes/solotoke.jpg","https://ourfore.st/img/femotes/1toker.gif"];

function getCommands(bot) {
  var commands = {

    "420blazeit": new Command({
      cmdName: "420blazeit",
      minRank: bot.RANKS.USER,
      rankMatch: ">=",
      userCooldown: 0,
      cmdCooldown: 0,
      isActive: true,
      requiredChannelPerms: ["chat"],
      allowRankChange: true,
      canBeUsedInPM: false
    }, function (cmd, user, message, opts) {
	    toke(user.name, bot, cmd);
    })


  }

  var aliases = {
    toak: "420blazeit",
    666: "420blazeit",
    420: "420blazeit",
    toke: "420blazeit",
    tokem: "420blazeit",
    toek: "420blazeit",
    hailsatan: "420blazeit",
    cheers: "420blazeit",
    toast: "420blazeit",
    toastem: "420blazeit",
    burn: "420blazeit",
    burnem: "420blazeit",
    lightem: "420blazeit",
    dab: "420blazeit",
    dabem: "420blazeit",
    smoke: "420blazeit",
    smokem: "420blazeit",
    blaze: "420blazeit",
    blazeit: "420blazeit",
    blazem: "420blazeit",//^og tokes
    drink: "420blazeit",
    shot: "420blazeit",
    weed: "420blazeit",
    marijuana: "420blazeit",
    cannabis: "420blazeit",
    jazzcabbage: "420blazeit",
    oktem: "420blazeit",
    puff: "420blazeit",
    hit: "420blazeit",
    tjoke: "420blazeit",
    tjokem: "420blazeit",
    devilslettuce: "420blazeit",
    tokem: "420blazeit",
    toakem: "420blazeit",
    grass: "420blazeit",
    liftoff: "420blazeit",
    smokeweed420blazeit: "420blazeit",
    smokeweed420blazem: "420blazeit",
    boof: "420blazeit",
    boofem: "420blazeit",
    tonk: "420blazeit",
    tonkem: "420blazeit",
    tonker: "420blazeit",
    bonghits4jesus: "420blazeit",
    tedcruzdid911: "420blazeit",
    epsteindidntkillhimself: "420blazeit",
    zillatoke: "420blazeit",
    ekot: "420blazeit",
    mekot: "420blazeit",
    smonk: "420blazeit",
    smonkem: "420blazeit",
    hash: "420blazeit",
    kush: "420blazeit",
    cheeseit: "420blazeit",
    munch: "420blazeit",
    munchem: "420blazeit",
    vape: "420blazeit",
    vapem: "420blazeit",
    fire: "420blazeit",
    firemup: "420blazeit",
    710: "420blazeit",
    roast: "420blazeit",
    roastem: "420blazeit"

	
  }

  return {commands: commands, aliases: aliases}
}

module.exports = {
  getCommands:getCommands
}

function toke(name, bot, message){
	/*if(name === "Ten"){//use in case of anger
		bot.sendChatMsg(">:^(");
		return;
	}*/
	switch (toking){
		case 0://ready to start toke
			bot.sendChatMsg("A group toke has been started by " + name + "! We'll be taking a toke in 60 seconds - join in by posting !" + message);
			cdown = 3;
			toking = 1;
			tokers.push(name);
			setTimeout(countdown, 57000, bot);
			break;
		case 1://taking toke
			if(tokers.includes(name)){
				bot.sendPM(name, ("You're already taking part in this toke!"));
			}else{
				bot.sendChatMsg(name + " joined the toke! Post !" + message + " to take part!");	
				tokers.push(name);
				cdown = 3;
			}
			break;
		case 2://cooldown
			bot.sendPM(name, "Please wait " + ctime + " before starting a new group toke.");
			break;
	}
}


function countdown(bot){
		toking = 1;//set toking mode
	
		bot.sendChatMsg(cdown + "...");//send countdown msg
		--cdown;//count down
		
		if(cdown <= 0){//if cdown hits 0
			setTimeout(endToke, 1000, bot);	
		}else{
			setTimeout(countdown, 1000, bot);//call endtoke
		}
}

function endToke(bot){
	if(cdown != 0){
		setTimeout(countdown, 1000, bot);
		return;
	}
	if(tokers.length > 1){
		let callstring = '';

		for(let i = 0; i < tokers.length - 1; i++){
			callstring += tokers[i] + ', ';
		}
		
		callstring += tokers[tokers.length - 1];

		bot.sendChatMsg("Take a toke " + callstring + "! " + tokers.length + " tokers!");
	}else{
		bot.sendChatMsg("Take a toke " + tokers.toString() + ". " + (solotokes[getRandomInt(0,solotokes.length)]));
	}
	tokers = [];
	toking = 2;//reset toking mode
	setTimeout(cooldown, 1000);
}

function cooldown(){
	if(ctime > 0){
		toking = 2;
		--ctime;
		setTimeout(cooldown, 1000);
	}else{
		toking = 0;
		ctime = cdel;

	}

}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
}
